# Tarea 1. Creación de una extensión para Visual Studio Code

## Información general

* **Fecha:** 28/04/2021
* **Alumno:** Héctor Hugo Sandoval Marcelo
* **Asignatura:** Computación en el Cliente Web
* **Repositorio:** [https://gitlab.com/hhsm95-maestria/gapline](https://gitlab.com/hhsm95-maestria/gapline)

## Objetivo

El objetivo es practicar la programación en JavaScript (conocer, además, TypeScript) y aplicarlo de manera novedosa. De manera paralela, vamos a trabajar con otras tecnologías modernas muy usadas hoy en día en el desarrollo web, tales como generadores y gestores de paquetes.

## Instrucciones

En cuanto a la entrega, se valorará una correcta narración de la ejecución del ejercicio paso a paso (instalación de componentes, edición de los ficheros, ejecución de la extensión con un ejemplo, etc.). Puedes incluir una captura de pantalla demostrando que la extensión funciona correctamente.

Contesta a todas las preguntas de manera ordenada y limpia. Se trata de un ejercicio propio de un master universitario y se tendrá en cuenta una correcta explicación y cuidado en la presentación. Cuida la ortografía y los nombres propios de las tecnologías usadas (no es vs Code, ni vs code, sino VS Code).

Presenta tu ejercicio en la plataforma en PDF o en un HTML online (con todo incrustado: imágenes, estilos, etc.). Es recomendable el uso de Markdown como estándar industrial para registrar información de tipo técnica.

## Software requerido

* [Node.js](https://nodejs.org/es/ "Node.js"). El cual nos servirá para instalar paquetes en nuestro sistema.
* [Visual Studio Code](https://code.visualstudio.com/ "VS Code"). Será nuestro editor de código.

## Instalación de paquetes

Necesitaremos instalar 3 paquetes para crear una extensión de VS Code:

|Paquete|Descripción|
|-|-|
|`yo`|También conocido como Yeoman, es una herramienta que nos ayuda a generar plantillas de proyectos y lo usaremos para la creación de los archivos y directorios necesarios para comenzar a codificar la extensión|
|`generator-code`|Es una extensión para Yeoman, le indicará las reglas que debe usar para la correcta creación de una plantilla de una extensión de VS Code|
|`typescript`|Es un complemento del lenguaje Javascript que le agrega un tipado estricto a la definición de variables y funciones|

Para instalar estas herramientas utilizaremos el comando:

`npm install -g yo generator-code typescript.`

Aquí utilizaremos el comando `npm` el cual es el gestor de paquetes que incluye Node.js y venía junto a su instalación.

![Instalación de paquetes](img/install.PNG "Instalación de paquetes")

## Creación del proyecto con Yeoman

Para instalar el proyecto con Yeoman se utilizó la siguiente instrucción:

`yo code`

Al ejecutar la instrucción, fue necesario configurar los siguientes parámetros:

|Parámetro|Descripción|Valor propuesto|Justificación|
|-|-|-|-|
|`What type of extension do you want to create?`|Tipo de aplicación que se desea crear|*New extension (Typescript)*|Utilizaremos Typescript en nuestra extensión|
|`What's the name of your extension?`|Nombre de la extensión|*Line Gapper*|Un nombre acorde a la funcionalidad de la extensión|
|`What's the identifier of your extension?`|Identificador para la extensión|*gapline*|Nombre corto parecido al nombre para identificarla|
|`What's the description of your extension?`|Descripción de la extensión|*Esta extensión va a realizar una tarea muy sencilla: insertar una línea en blanco cada N líneas.*|Describe lo que hará nuestra extensión|
|`Initialize a Git repository?`|Inicializar un repositorio de Git|*No*|De momento no utilizaremos un repositorio|
|`Bundle the source code with Webpack?`|Empaquetar el código con Webpack|*No*|De momento no es necesario utilizar Webpack|
|`Which package manager to use?`|Manejador de paquetes a utilizar|*npm*|Utilizaremos el manejador de paquetes npm|

Al ejecutar se obtendrá un resultado parecido al siguiente:

![Creación de la extensión](img/extension.PNG "Creación de la extensión")

Con el proyecto creado, fue necesario abrirlo en el programa VS Code para continuar la siguiente parte del ejercicio.

## Estructura del proyecto

Al abrir el proyecto en VS Code, notaremos la siguiente estructura:

![Estructura del proyecto](img/estructura.PNG "Estructura del proyecto")

|Elemento|Descripción|
|-|-|
|`.vscode`|Aquí se almacenan las configuraciones de VS Code para este proyecto en específico|
|`node_modules`|Aquí se encuentran los binarios de las dependencias instaladas por npm para el funcionamiento del proyecto|
|`src`|Aquí alojaremos el código fuente de nuestro proyecto|
|`src/test`|Aquí escribiremos los tests del proyecto|
|`src/extension.ts`|Este es el archivo donde se encuentra el punto de entrada de nuestro programa y sobre el cual vamos a trabajar|
|`.eslintrc.json`|Este archivo contiene las reglas de sintaxis que utilizará VS Code para alertarnos en caso de fallar en alguna mientras programamos|
|`.vscodeignore`|En este archivo se especifican los archivos que serán ignorados al momento de instalar la extensión|
|`CHANGELOG.md`|Este archivo sirve para documentar los cambios que hagamos al proyecto|
|`package-lock.json`|Este archivo contiene todo el historial de dependencias instaladas por npm|
|`package.json`|Este archivo contiene información básica del proyecto, el archivo de entrada, algunos scripts que se pueden ejecutar, y también las dependencias instaladas por npm|
|`README.md`|En este archivo se describe información acerca del proyecto, como la descripción del mismo, motivación para crear el mismo, su objetivo, las instrucciones de instalación, entre otras cosas|
|`tsconfig.json`|Este archivo se encarga de establecer las configuraciones de TypeScript|
|`vsc-extension-quickstart.md`|Este archivo contiene información para comprender el funcionamiento de las extensiones de VS Code|

## Ejercicio inicial

Comenta cada línea del código `src/extension.ts` teniendo en cuenta el propósito descrito al principio de este enunciado.

Busca en el API de las extensiones de VS Code [https://code.visualstudio.com/api/references/vscode-api](https://code.visualstudio.com/api/references/vscode-api) y averigua para qué sirven las funciones activate, deactivate y el resto de funciones y propiedades específicas de las extensiones de VS Code.

Es importante que entiendas cada línea de código. Si tienes alguna dificultad, puedes preguntar en el foro ad hoc para la resolución del ejercicio. ¿Qué elementos específicos de TypeScript identificas? Recuerda que el profesor está para ayudarte y que esta ayuda continua en el foro.

### Código de la extensión comentado

En este ejercicio se utiliza TypeScript para resaltar el tipo de variables que estemos utilizando y que de esta manera VS Code pueda sugerirnos sus métodos/funciones.

```typescript
"use strict";
import * as vscode from "vscode";

/* activate function:
Se ejecuta cuando la extensión es activada por primera vez.
Básicamente se prepara todo para el arranque de la extensión.
*/
export function activate(context: vscode.ExtensionContext) {
  /* registerCommand function:
  Sirve para dar de alta los comandos que podrán ser utilizados en VS Code como parte de nuestra extensión,
  y pueden ser ejecutados desde la Paleta de Comandos (CTRL + SHIFT + P) si registramos el ID del comando en
  el archivo package.json en el objeto contributes.commands.
  */
  let disposable = vscode.commands.registerCommand("extension.gapline", () => {
    // Asigna en una variable el editor actual sobre el que estemos posicionados, en caso contrario regresará undefined.
    const editor = vscode.window.activeTextEditor;
    if (!editor) {
      return;
    }

    // Almacena el texto seleccionado del editor.
    const selection = editor.selection;
    let text = editor.document.getText(selection);

    /* showInputBox promise:
    Muestra una caja de texto solicitando información al usuario (en este caso número de líneas).
    Y continúa su ejecución si recibe datos a través de una función then.
    */
    vscode.window.showInputBox({ prompt: "Lineas?" }).then((value) => {
      let numberOfLines = +value;
      const textInChunks: Array<string> = [];
      // Del texto seleccionado, por cada n líneas agrega una línea en blanco.
      text.split("\n").forEach((currentLine: string, lineIndex) => {
        textInChunks.push(currentLine);
        if ((lineIndex + 1) % numberOfLines === 0) {
          textInChunks.push("");
        }
      });
      // Junta todo el resultado nuevamente en un solo texto con las líneas originales y en blanco.
      text = textInChunks.join("\n");
      // Reemplaza el texto seleccionado originalmente con el nuevo texto que contiene líneas en blanco.
      editor.edit((editBuilder) => {
        const range = new vscode.Range(
          selection.start.line,
          0,
          selection.end.line,
          editor.document.lineAt(selection.end.line).text.length
        );
        editBuilder.replace(range, text);
      });
    });
  });
  // Suscribe el comando para que pueda ser utilizado por VS Code.
  context.subscriptions.push(disposable);
}

/* deactivate function:
Se ejecuta cuando la extensión es desactivada.
Aquí se podría hacer limpieza de archivos cache o servicios que pudiésemos estar consumiendo y que se requiera terminar.
*/
export function deactivate() {}
```

## Segundo ejercicio

Comprueba que tu extensión funciona. Para ello, primero asegúrate de que sustituyes todos los sayHello (que inserta por defecto el generador code que hemos aplicado con Yeoman) por el nombre de la función que hemos usado (gapline en nuestro caso) en el fichero package.json. Como hemos comentado antes, este fichero contiene las opciones e información básicas de nuestra extensión y, entre otras cosas, cómo ejecutarla. Son especialmente importantes las secciones activationEvents y commands. Vigila que el nombre del comando sea el correcto.

Para ejecutar la extensión, pulsa F5 (o la tecla función + F5, dependiendo de tu teclado y sistema) o selecciona la opción de menú Start debugging del menú Debug. Verás que se abre una nueva ventana de VS Code (que ya tiene tu extensión precargada).

Abre un fichero de texto cualquiera, selecciona su contenido y pulsa control/comando + shift + P.

### Resultado

Capturas del funcionamiento.

Seleccionar el texto.

![Seleccionar el texto](img/1.PNG "Seleccionar el texto")

Abrir la paleta de comandos con CTRL + SHIFT + P y buscar nuestra extensión.

![Buscar extensión](img/2.PNG "Buscar extensión")

Escribir en la caja en número de líneas.

![Escribir en la caja en número de líneas](img/3.PNG "Escribir en la caja en número de líneas")

Ver el resultado.

![Resultado](img/4.PNG "Resultado")
