"use strict";
import * as vscode from "vscode";

/* activate function:
Se ejecuta cuando la extensión es activada por primera vez.
Básicamente se prepara todo para el arranque de la extensión.
*/
export function activate(context: vscode.ExtensionContext) {
  /* registerCommand function:
  Sirve para dar de alta los comandos que podrán ser utilizados en VS Code como parte de nuestra extensión,
  y pueden ser ejecutados desde la Paleta de Comandos (CTRL + SHIFT + P) si registramos el ID del comando en
  el archivo package.json en el objeto contributes.commands.
  */
  let disposable = vscode.commands.registerCommand("extension.gapline", () => {
    // Asigna en una variable el editor actual sobre el que estemos posicionados, en caso contrario regresará undefined.
    const editor = vscode.window.activeTextEditor;
    if (!editor) {
      return;
    }

    // Almacena el texto seleccionado del editor.
    const selection = editor.selection;
    let text = editor.document.getText(selection);

    /* showInputBox promise:
    Muestra una caja de texto solicitando información al usuario (en este caso número de líneas).
    Y continua su ejecución si recibe datos a través de una función then.
    */
    vscode.window.showInputBox({ prompt: "Lineas?" }).then((value) => {
      let numberOfLines = +value;
      const textInChunks: Array<string> = [];
      // Del texto seleccionado, por cada n líneas agrega una en blanco.
      text.split("\n").forEach((currentLine: string, lineIndex) => {
        textInChunks.push(currentLine);
        if ((lineIndex + 1) % numberOfLines === 0) {
          textInChunks.push("");
        }
      });
      // Junta todo el resultado nuevamente en un solo texto con las líneas originales y en blanco.
      text = textInChunks.join("\n");
      // Reemplaza el texto seleccionado originalmente con el nuevo texto que contiene líneas en blanco.
      editor.edit((editBuilder) => {
        const range = new vscode.Range(
          selection.start.line,
          0,
          selection.end.line,
          editor.document.lineAt(selection.end.line).text.length
        );
        editBuilder.replace(range, text);
      });
    });
  });
  // Suscribe el comando para que pueda ser utilizado por VS Code.
  context.subscriptions.push(disposable);
}

/* deactivate function:
Se ejecuta cuando la extensión es desactivada.
Aquí se podría hacer limpieza de archivos cache o servicios que pudiésemos estar consumiendo y que se requiera terminar.
*/
export function deactivate() {}
