"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const vscode = require("vscode");
/* activate function:
Se ejecuta cuando la extensión es activada por primera vez.
Básicamente se prepara todo para el arranque de la extensión.
*/
function activate(context) {
    /* registerCommand function:
    Sirve para dar de alta los comandos que podran ser utilizados en VS Code como parte de nuestra extensión,
    y pueden ser ejecutados desde la Paleta de Comandos (CTRL + SHIFT + P) si registramos el ID del comando en
    el archivo package.json en el objeto contributes.commands.
    */
    let disposable = vscode.commands.registerCommand("extension.gapline", () => {
        // Asigna en una variable el editor actual sobre el que estemos posicionados, en caso contrario regresará undefined.
        const editor = vscode.window.activeTextEditor;
        if (!editor) {
            return;
        }
        const selection = editor.selection;
        let text = editor.document.getText(selection);
        /* showInputBox promise:
        Muestra una caja de texto solicitando información al usuario (en este caso número de líneas).
        Y continua su ejecución si recibe datos a través de una función then en cadena.
        */
        vscode.window.showInputBox({ prompt: "Lineas?" }).then((value) => {
            let numberOfLines = +value;
            const textInChunks = [];
            text.split("\n").forEach((currentLine, lineIndex) => {
                textInChunks.push(currentLine);
                if ((lineIndex + 1) % numberOfLines === 0) {
                    textInChunks.push("");
                }
            });
            text = textInChunks.join("\n");
            editor.edit((editBuilder) => {
                const range = new vscode.Range(selection.start.line, 0, selection.end.line, editor.document.lineAt(selection.end.line).text.length);
                editBuilder.replace(range, text);
            });
        });
    });
    context.subscriptions.push(disposable);
}
exports.activate = activate;
/* deactivate function:
Se ejecuta cuando la extensión es desactivada.
Aquí se podría hacer limpieza de archivos cache o servicios que pudiesemos estar consumiendo y que se requiera terminar.
*/
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map